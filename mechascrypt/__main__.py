import os
import sys

from mechascrypt.parser import parse

TEST = lambda t: print('yes') if t else print('no')

def repl():
    while True:
        try:
            s = input('> ')
        except EOFError:
            break
        print(parse(s))

def test():
    print(parse('UNLEASH >>FORCE<<'))
    print(parse('CAST TEMPORIUM SET >>LOS ANGELES<<'))

if __name__ == '__main__':
    if len(sys.argv) == 1:
        repl()
    else:
        target = sys.argv[1]
        if os.path.exists(target):
            program = parse(open(target).read(), execute=True)
            # print(ast.dump(program))
            exec(compile(program, filename="<ast>", mode="exec"))
        elif target == 'test':
            test()
        else:
            program = parse(target, execute=False)
            # print(ast.dump(program))
            print(eval(compile(program, filename="<ast>", mode="eval")))