__version__ = '0.1.0'

__doc__ = f"""
MECHASCRYPT v{__version__}, a python-subset language, makes the MECHAMANCERS possible.

MECHASCRYPT is primarily used to call functions, which is done by using the `CAST [FUNCTION NAME]` syntax. (e.g. `CAST INFO`)

Arguments are supplied by using the `[PARAMETER NAME] [ARGUMENT VALUE]` syntax. (e.g. `CAST TEMPORIUM ON >> @User << AT >>10:00 PM<<`)

For information regarding a specific function, use `CAST INFO INFORMATIC >> [FUNCTION NAME] <<`.
"""

from mechascrypt.parser import parse

import mechascrypt.exceptions

import mechascrypt.library

import mechascrypt.wrap

PARSABLE_INSTRUCTIONS = ['CAST']
