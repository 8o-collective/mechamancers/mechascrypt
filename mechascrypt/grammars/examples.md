[UNLEASH] CAST TEMPORIUM AT 2 COMPUTE AFFIRMATIVE

Expr(
    value=Call(
        func=Name(
            id='TEMPORIUM', 
            ctx=Load()
        ), 
        args=[], 
        keywords=[
            keyword(
                arg='at', 
                value=Constant(value=2)
            ), 
            keyword(
                arg='compute', 
                value=Constant(value=True)
            )
        ]
    )
)

--------------------------------

UNLEASH >>TEST<<

Expr(
    value=Call(
        func=Name(
            id='print', 
            ctx=Load()
        ), 
        args=[Constant(value='test')], 
        keywords=[]
    )
)

--------------------------------

MAKE TESTVARIABLE BE

Assign(
    targets=[
        Name(id='TESTVARIABLE', ctx=Store())
    ], 
    value=Constant(value=None)
)

--------------------------------

CAST TEMPORIUM SET >>LOS ANGELES<< 

Expr(
    value=Call(
        func=Name(
            id='temporium', 
            ctx=Load()
        ), 
        args=[], 
        keywords=[
            keyword(
                arg='set', 
                value=Constant(value='LOS ANGELES')
            )
        ]
    )
)

--------------------------------

FILL TESTVARIABLE WITH [CAST TEMPORIUM]

Assign(
    targets=[
        Name(
            id='TESTVARIABLE', 
            ctx=Store()
        )
    ], 
    value=Call(
        func=Name(
            id='TEMPORIUM', 
            ctx=Load()
        ), 
        args=[], 
        keywords=[]
    )
)

--------------------------------

FILL TESTVARIABLE WITH CAST TESTFUNC INPUT [CAST TESTGENERATE]

Assign(
    targets=[
        Name(
            id='TESTVARIABLE', 
            ctx=Store()
        )
    ], 
    value=Call(
        func=Name(
            id='TESTFUNC', 
            ctx=Load()
        ), 
        args=[], 
        keywords=[
            keyword(
                arg='INPUT', 
                value=Call(
                    func=Name(
                        id='TESTGENERATE', 
                        ctx=Load()
                    ), 
                    args=[], 
                    keywords=[]
                )
            )
        ]
    )
)

--------------------------------

CAST print VOID >>test<<

Expr(
    value=Call(
        func=Name(
            id='print', 
            ctx=Load()
        ), 
        args=[
            Constant(value='test')
        ], 
        keywords=[]
    )
)

---------------------------------

SHOULD TEST == 1 PERMIT 
OPEN 
    UNLEASH >>HELLO WORLD<< 
FIN

If(
    test=Compare(
        left=Name(id='TEST', ctx=Load()), 
        ops=[Eq()], 
        comparators=[
            Constant(value=1)
        ]
    ), 
    body=[
        Expr(
            value=Call(
                func=Name(id='print', ctx=Load()), 
                args=[Constant(value='HELLO WORLD')], 
                keywords=[])
            )
    ], 
    orelse=[]
)
