import inspect

from mechascrypt import __doc__

from mechascrypt.exceptions import CommandNotFoundError, DocsNotFoundError

async def info(informatic=None):
	'''
	The information which you seek lies right in front of your very eyes.

	Parameters
	----------
	informatic: str
		name of the command to get information of
	'''

	bot = inspect.getmodule(inspect.stack()[1][0]) # hacky but works

	if informatic == None and getattr(bot, "patron", False):
		return __doc__
	
	if isinstance(informatic, str):
		informatic = informatic.lower()

		if informatic == bot.__name__:
			return bot.__doc__

		symbol = getattr(getattr(bot.commands, informatic, None), informatic, None) # get function object

		if symbol is None and getattr(bot, "patron", False):
			try:
				symbol = __import__(f'mechascrypt.library.{informatic}', fromlist=[informatic]).__dict__.get(informatic)
			except:
				pass
	elif hasattr(informatic, "__doc__"):
		symbol = informatic
	else:
		raise CommandNotFoundError

	if symbol:
		if symbol.__doc__:
			return symbol.__doc__
		else:
			raise DocsNotFoundError 
	else:
		raise CommandNotFoundError