import os
import ast

from lark import Lark, Transformer

grammars = os.path.join(os.path.dirname(os.path.realpath(__file__)), "grammars")
grammar = open(os.path.join(grammars, "primitive.lark")).read()
parser = Lark(grammar, parser='lalr')

## Define transformer
class ToAst(Transformer):
    def BOOL(self, b):
        return ast.Constant(
            value=(b == 'AFFIRMATIVE'),
            lineno=b.line, col_offset=b.column
        )
    
    def NUMBER(self, n):
        return ast.Constant(
            value=float(n),
            lineno=n.line, col_offset=n.column
        )

    def STRING(self, s):
        return ast.Constant(
            value=s[2:-2], 
            lineno=s.line, col_offset=s.column
        )

    def NAME(self, n):
        return ast.Name(
            id=str(n), 
            ctx=ast.Load(),
            lineno=n.line, col_offset=n.column
        )

    def unleash(self, u):
        return ast.Call(
            func=ast.Name(
                id='print', 
                ctx=ast.Load(),
            ), 
            args=u,
            keywords=[],
        )
    
    def cast(self, c):
        params = iter(c[1:])

        return ast.Call(
            func=c[0],
            args=[],
            keywords=[
                ast.keyword(
                    arg=param.id.lower(), 
                    value=value, 
                ) for (param, value) in zip(params, params)
            ],
        )

    def declare(self, d):
        for node in d: node.ctx = ast.Store()

        return ast.Assign(
            targets=d,
            value=ast.Constant(
                value=None, 
                lineno=d[0].lineno, 
            ),
        )
    
    def assign(self, a):
        for node in a: node.ctx = ast.Store()

        return ast.Assign(
            targets=a[:-1], 
            value=a[-1],
        )
    
    def code_block(self, c):
        pass
        

    def if_statement(self, c):
        pass        
        
    
    # def bool_math(self, c):
        
    def start(self, s):
        if self.execute:
            e = [ast.Expr(node) for node in s if issubclass(type(node), ast.expr)]
            for node in e: ast.fix_missing_locations(node)
            return ast.Module(body=e, type_ignores=[])

        e = s[0]
        # e = ast.Expr(v) if issubclass(v, ast.expr) else v
        ast.fix_missing_locations(e)
        return ast.Expression(body=e)


def parse(data, execute=False):
    tree = parser.parse(data)
    transformer = ToAst()
    transformer.execute = execute
    return transformer.transform(tree)