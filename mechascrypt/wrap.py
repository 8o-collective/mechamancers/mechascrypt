def outer(command, message=None):
    async def inner(*args, **kwargs):
        if message:
            kwargs['message'] = message

        return await command(*args, **kwargs)
    
    inner.__doc__ = command.__doc__
    inner.__name__ = command.__name__.upper()

    return inner

def wrap(commands, message=None):
	return [outer(command, message) for command in commands]