from lark.exceptions import *

class CommandNotFoundError(ModuleNotFoundError):
	pass

class DocsNotFoundError(ModuleNotFoundError):
	pass